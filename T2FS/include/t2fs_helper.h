#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "t2fs.h"

#define MAX_OPEN_FILES 20
#define BLOCK_SIZE (g_boot_block.blockSize * SECTOR_SIZE)

typedef struct t2fs_bootBlock BOOT_BLOCK;
typedef struct t2fs_4tupla MFT_TUPLA;
typedef struct t2fs_record RECORD;

typedef struct {
    bool valid;
    char *file_name;
    char *file_full_path;
    unsigned int file_size;
    unsigned int current_position;
    unsigned int mft_number;
    unsigned int record_offset;
} FILE_DESCRIPTOR;

typedef struct {
    bool valid;
    char *dir_name;
    char *dir_full_path;
    unsigned int dir_size;
    unsigned int current_position;
    unsigned int mft_number;
    unsigned int record_offset;
} DIR_DESCRIPTOR;

typedef struct {
	MFT_TUPLA mft_tuplas[32];
} MFT_REGISTER;

BOOT_BLOCK g_boot_block;
FILE_DESCRIPTOR g_open_files[MAX_OPEN_FILES];
DIR_DESCRIPTOR *g_open_dirs;
int g_largest_dir_handle;
bool g_lib_init;

void initLibrary(void);
bool isFileOpenByHandle(FILE2 handle);
bool isFileOpenByFullPath(const char *full_path);
void removeFromOpenFiles(FILE2 handle);
bool isDirOpenByHandle(DIR2 handle);
bool isDirOpenByFullPath(const char *full_path);
void removeFromOpenDirs(DIR2 handle);
void dumpBootBlockInfo(void);
void dumpDirEntryInfo(DIRENT2 *dentry);
void dumpDirRecordInfo(RECORD *dentry);
void dumpFileDescriptorInfo(FILE_DESCRIPTOR *open_file);
void dumpDirDescriptorInfo(DIR_DESCRIPTOR *open_dir);
void dumpOpenFiles(void);
void dumpOpenDirs(void);
void dumpMftRegisterInfo(MFT_REGISTER *mft_register);
void dumpMftTuplaInfo(MFT_TUPLA *mft_tupla);
int getMftTuplaIndexUsingVBN(DWORD VBN, MFT_REGISTER mft_reg);
unsigned int findRegisterWithVBN(DWORD VBN, unsigned int start_mft_number);
unsigned int findTuplaInRegisterWithVBN(DWORD VBN, MFT_REGISTER mft_reg);
unsigned int findLBNInRegisterWithVBN(DWORD VBN, MFT_REGISTER mft_reg);
bool is4tuplaEnded(MFT_TUPLA _4tupla, int number_of_swept_sectors);
MFT_REGISTER mftRegisterFromSector(unsigned int sector_number);
MFT_REGISTER mftRegisterFromMftNumber(unsigned int mft_number);
unsigned int getFirstFreeMftNumber(void);
void readBlockByLBN(unsigned char *block, unsigned int LBN);
void writeBlockToLBN(unsigned char *block, unsigned int LBN);
RECORD readRecordFromBlockByNumber(unsigned char *block_buf, unsigned int record_num);
RECORD readRecordFromDiskOffset(unsigned int disk_offset);
int writeRecordToDiskOffset(RECORD *record, unsigned int disk_offset);
int invalidateRecordByDiskOffset(unsigned int disk_offset);
unsigned int diskOffsetFromMftRegisterByNameAndType(MFT_REGISTER mft_register, char *name, int type);
unsigned int dirDiskOffsetFromMftRegisterByName(MFT_REGISTER mft_register, char *name);
unsigned int fileDiskOffsetFromMftRegisterByName(MFT_REGISTER mft_register, char *name);
void markFreeBlocksInBitmap(int LBN, int contig_blocks);
bool isNextContiguousBlockFree(MFT_TUPLA mft_tupla);
int allocateContiguousBlock(MFT_TUPLA *mft);
FILE2 findFirstFreeFileHandle(void);
DIR2 findFirstFreeDirHandle(void);
DIR2 findLastOccupiedDirHandle(void);
FILE2 openFileUsingDirRecord(RECORD dir_record, char *file_full_path, DWORD record_offset);
DIR2 openDirUsingDirRecord(RECORD dir_record, char *dir_full_path, DWORD record_offset);
DIR2 openRootDir(void);
RECORD getRootDirRecord(void);
RECORD getLastDirRecordFromPath(char *pathname);
void fillDirEntryFromRecord(RECORD *record, DIRENT2 *dentry);
void writeRegister(MFT_REGISTER mft_register, unsigned int mft_number);
void createMftTupla(int mft_number);
void allocateMftTupla(int mft_number);
void freeAllBlocksAndTuplasInRegisterByVBN(unsigned int start_mft_number,
										   unsigned int first_unneeded_VBN,
										   bool invalidate_before_recurse);
bool isDirEmpty(RECORD dir);
unsigned int findFirstFreeDiskOffsetForRecordInDirRecord(RECORD dir);
DWORD getLBNUsingVBN(DWORD mft_number, DWORD VBN);
DWORD getLBNUsingVBNAndMftTupla(DWORD mft_number, int tupla_index, DWORD VBN);
int getEndingTuplaIndexInMftRegister(MFT_REGISTER mft_reg);
int allocateBlocksInMft(MFT_REGISTER mft_reg, int mft_number, int number_of_blocks, DWORD VBN);
void writeDataUsingMFT(MFT_REGISTER mft_reg, int starting_tupla, DWORD starting_VBN, DWORD starting_byte, char *buffer, int size, bool new_blocks);
