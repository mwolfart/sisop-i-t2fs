#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "t2fs_helper.h"
#include "apidisk.h"

int main() {
    g_open_files[0].valid = true;
    g_open_files[0].file_name = malloc(50*sizeof(char));
    strcpy(g_open_files[0].file_name, "file1.dat");
    g_open_files[0].file_full_path = malloc(500*sizeof(char));
    strcpy(g_open_files[0].file_full_path, "/home/file1.dat");
    g_open_files[0].file_size = 50;
    g_open_files[0].current_position = 0;

    g_open_files[1].valid = true;
    g_open_files[1].file_name = malloc(50*sizeof(char));
    strcpy(g_open_files[1].file_name, "file2.dat");
    g_open_files[1].file_full_path = malloc(500*sizeof(char));
    strcpy(g_open_files[1].file_full_path, "/home/file1.dat");
    g_open_files[1].file_size = 100;
    g_open_files[1].current_position = 0; 

	FILE2 f1 = 0;
	FILE2 f2 = 1;
	FILE2 f3 = 2;

    if (seek2(f1, 20) != 0)
        printf("error in first seek\n");

    if (seek2(f2, -1) != 0)
        printf("error in second seek\n");

    if (seek2(f1, 200) != 0)
        printf("success: third seek must return error\n");

    if (close2(f1) != 0)
        printf("error in closing file1\n");

    if (close2(f2) != 0)
        printf("error in closing file2\n");

    if (close2(f3) != 0)
        printf("success: closing file3 must return error\n");

    if (seek2(f1, 20) != 0)
        printf("success: fourth seek must return an error\n");
    
    return 0;
}
