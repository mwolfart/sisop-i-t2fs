#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "t2fs_helper.h"
#include "apidisk.h"

int main() {
	DIR2 root_handle = opendir2("/");
	printf("opendir \"/\" returned: %d\n", root_handle);

	DIRENT2 dentry;
	int ret = readdir2(root_handle, &dentry);
	printf("readdir returned: %d\n", ret);
	dumpDirEntryInfo(&dentry);

	ret = readdir2(root_handle, &dentry);
	printf("readdir returned: %d\n", ret);
	dumpDirEntryInfo(&dentry);

	ret = readdir2(root_handle, &dentry);
	printf("readdir returned: %d\n", ret);
	dumpDirEntryInfo(&dentry);

	ret = readdir2(root_handle, &dentry);
	printf("readdir returned: %d\n", ret);
	dumpDirEntryInfo(&dentry);

	ret = readdir2(root_handle, &dentry);
	printf("readdir returned: %d\n", ret);
	dumpDirEntryInfo(&dentry);

    return 0;
}
